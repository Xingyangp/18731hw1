/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include "statefulfirewall.hh"


//Debug
#define USEDEBUG
#ifdef USEDEBUG
#define Debug( x ) std::cout << x 
#else
#define Debug( x ) 
#endif

/* Add header files as required*/
CLICK_DECLS

//Add your implementation here.
Connection::Connection(String s, String d, int sp, int dp, unsigned long seq_s, unsigned long seq_d, int pr, bool fwdflag)
{
    //Add your implementation here.
	this->sourceip = s;
    this->destip = d;
    this->sourceport = sp;
    this->destport = dp;
    this->sourceseq = seq_s;
    this->destseq = seq_d;
    this->proto = pr;
    this->isfw = fwdflag;
    this->handshake_stat = SYN;
  	
}
Connection::Connection() {}
Connection::~Connection() {}

/* Can be useful for debugging*/
void Connection::print() const
{
	click_chatter("------CONNECTION DESCRIBE--------");
	// click_chatter("Connection from source ip %s, source port %d, source seq %lu", sourceip.c_str(),sourceport,sourceseq);
	// click_chatter("To destination ip %s, destination port %d, destination seq %lu", destip.c_str(),destport,destseq);
	// click_chatter("Protocol is %d", proto);
	click_chatter("srcip:%s,srcport:%d,srcseq:%lu,dstip:%s,dstport:%d,destseq:%lu,proto:%d,forward:%d,handshake_stat:%d ",sourceip.c_str(),sourceport,sourceseq,destip.c_str(),destport,destseq,proto,isfw,handshake_stat);

	//click_chatter("Forward State is %d", isfw);
	// click_chatter("Handshake State is %d",handshake_stat);
	click_chatter("------CONNECTION DESCRIBE END--------");


}

/* Overlaod == operator to check if two Connection objects are equal.
* You may or may not want to ignore the isfw flag for comparison depending on your implementation.
* Return true if equal. false otherwise. */
bool Connection::operator==(const Connection &other) const
{
	//click_chatter("------COMPARING START ------");
	//print();
	//other.print();
	//click_chatter("------COMPARING END ------");
	if(this->sourceip != other.sourceip) return false;
	if(this->sourceport != other.sourceport) return false;
	if(this->destip != other.destip) return false;
	if(this->destport != other.destport) return false;
	if(this->proto != other.proto) return false;
	//if(this.isfw != other.isfw) return false;
	//if(this.handshake_stat != other.handshake_stat ) return false;

	return true;

}

/*Compare two connections to determine the sequence in map.*/
int Connection::compare(const Connection other) const
{
	if(this->sourceport < other.sourceport) return -1;
	if(this->destport < other.destport) return -1;
	if(this->sourceseq < other.sourceseq) return -1;
	if(this->destseq < other.destseq) return -1;
	return 1;

}

unsigned long Connection::get_sourceseq() 
{
	return this->sourceseq;
}

unsigned long Connection::get_destseq()
{
    return this->destseq;
}

void Connection::set_sourceseq( unsigned long seq_s )
{
    this->sourceseq = seq_s;
}

void Connection::set_destseq( unsigned long seq_d )
{
    this->destseq = seq_d;
}

int Connection::get_handshake_stat()
{
    return this->handshake_stat;
}

/* Update the status of the handshake */
void Connection::update_handshake_stat(int hs)
{
    this->handshake_stat=hs;
}
/* Return value of isfw*/
bool Connection::is_forward()
{
	return isfw;
}

void Connection::update_forward()
{
	isfw = !isfw;
}
Policy::Policy(String s, String d, int sp, int dp, int p, int act)
{
	this->sourceip = s;
    this->destip = d;
    this->sourceport = sp;
    this->destport = dp;
    this->proto = p;
    this->action = act;
}
Policy::~Policy(){}
/* Return a Connection object representing policy */
Connection Policy::getConnection()
{
	
	if(proto!=IP_PROTO_TCP||sourceip.compare(destip)<0)
	{
		return Connection(sourceip,destip,sourceport,destport,0,0,proto,true);
	}
	else
	{
		return Connection(destip,sourceip,destport,sourceport,0,0,proto,false);
	}
}
/* Return action for this Policy */
int Policy::getAction()
{
	return action;
}

StatefulFirewall::StatefulFirewall(){}
StatefulFirewall::~StatefulFirewall(){}


    /* Take the configuration paramenters as input corresponding to
     * POLICYFILE and DEFAULT where
     * POLICYFILE : Path of policy file
     * DEFAULT : Default action (0/1)
     *
     * Hint: Refer to configure methods in other elemsnts.
     */
int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
	
	String policy_path;
	int action;

	
	if(Args(conf,this,errh)
		.read_mp("POLICYFILE",policy_path)
		.read_mp("DEFAULT",action)
		.complete() < 0)
		return -1;

	//click_chatter("Policyfile path is %s",policy_path.c_str());
	//click_chatter("Default Action is %d",action);

	if(action!=0 && action!=1) return errh->error("Action invalid. Must be 0/1");
	if(read_policy_config(policy_path)==-1) return errh->error("Bad policy path");	
	DEFAULTACTION = action; 
	return 0;


}
	/* Read policy from a config file whose path is passed as parameter.
     * Update the policy database.
     * Policy config file structure would be space separated list of
     *##SRCIP SRCPORT DSTIP DSTPORT PROTO ACTION
     * <source_ip source_port destination_ip destination_port protocol action>
     * Add Policy objects to the list_of_policies
     * return -1 if path invalid.
     * return 0 if success.
     * */
int StatefulFirewall::read_policy_config(String s)
{
	ifstream infile;
	infile.open(s.c_str());
	//SRCIP SRCPORT DSTIP DSTPORT PROTO ACTION
	string sip="";
	string dip="";
	uint16_t sport,dport;
	int proto;
	int action;
	//if(!infile.is_open()) return -1;
	//Policy::Policy(String s, String d, int sp, int dp, int p, int act)
	string line;
	while(std::getline(infile,line))// >> sip >> sport >> dip >> dport >> proto >> action)
	{
		if(strncmp(line.c_str(), "##", 2)==0) continue; //first line
		if(line.length()<5) continue; //empty line
		std::istringstream words(line);
		words >> sip >> sport >> dip >> dport >> proto >> action;
		String sourceip(sip.c_str());
		String destip(dip.c_str());
		list_of_policies.push_back(Policy(sourceip,destip,sport,dport,proto,action));
		//click_chatter("------POLICIES ARE------");
		//click_chatter("srcip:%s,srcport:%d,dstip:%s,dstport:%d,proto:%d,action:%d,",sourceip.c_str(),sport,destip.c_str(),dport,proto,action);

	}
	return 0;
}

/* Convert the integer ip address to string in dotted format.
 * Store the string in s.
 *
 * Hint: ntohl could be useful.*/
void StatefulFirewall::dotted_addr(const uint32_t *addr, char *s)
{

}
/* Create a new connection object for Packet.
 * Make sure you canonicalize the source and destination ip address and port number.
 * i.e, make the source less than the destination and
 * update isfw to false if you have to swap source and destination.
 * return NULL on error. */
//Connection(String s, String d, int sp, int dp, unsigned long seq_s, unsigned long seq_d, int pr, bool fwdflag)

Connection StatefulFirewall::get_canonicalized_connection(const Packet *p)
{
	if(!p->has_network_header()) return Connection();
	const click_ip *iph = p->ip_header();
	uint8_t pro = iph->ip_p; 
	struct in_addr ip_src = iph->ip_src;
	struct in_addr ip_dst = iph->ip_dst;
	String s = inet_ntoa(ip_src);
	String d = inet_ntoa(ip_dst);
	int pr = pro;
	int sp = 0;
	int dp = 0;
	unsigned long seq_s = 0;
	unsigned long seq_d = 0;
	bool fwdflag = true;

	if(pr == IP_PROTO_TCP)
	{	
		if(!p->has_transport_header())
		{
			return Connection();
		}
		const click_tcp *tcph = p->tcp_header();
		//reverse 
		if(s.compare(d)>0)
		{
			fwdflag = false;
			s = inet_ntoa(ip_dst);
			d = inet_ntoa(ip_src);
			dp = ntohs(tcph->th_sport);
			sp = ntohs(tcph->th_dport);			
			seq_d = ntohl(tcph->th_seq);
			seq_s = ntohl(tcph->th_ack);
		}
		else{

			sp = ntohs(tcph->th_sport);
			dp = ntohs(tcph->th_dport);
			seq_s = ntohl(tcph->th_seq);
			seq_d = ntohl(tcph->th_ack);
		}
		
	    
		
	}
	else if(pr == IP_PROTO_UDP)
	{
		if(!p->has_transport_header())
		{
			return Connection();
		}
		const click_udp *udph = p->udp_header();
		sp = ntohs(udph->uh_sport);
		dp = ntohs(udph->uh_dport);

	}
	return Connection(s,d,sp,dp,seq_s,seq_d,pr,fwdflag);
	



}

/* return true if Packet represents a new connection
 * i.e., check if the connection exists in the map.
 * You can also check the SYN flag in the header to be sure.
 * else return false.
 * Hint: Check the connection ID database.
 */

bool StatefulFirewall::check_if_new_connection(const Packet *p)
{
	//find connection
	Connection cnt = get_canonicalized_connection(p);
	//click_chatter("-----STEP 2 :compare from chech new conn-----");
	std::map<Connection,int>::iterator it;
	for(it = Connections.begin(); it != Connections.end(); it++ )
	{
		if(it->first == cnt) return false;
	
	}
	//chech SYN flag
	if(!p->has_network_header()) return false;
	const click_ip *iph = p->ip_header();
	uint8_t pr = iph->ip_p; 
	if(pr == IP_PROTO_TCP)
	{	
		if(!p->has_transport_header())
		{
			return false;
		}
		const click_tcp *tcph = p->tcp_header();
		//consider fin rst syn ack flags
		if((tcph->th_flags & 0x17)== TH_SYN) return true;

	}
	return false;
}

/*check if handshake process is stricty followed
 *update connection stat if valid
 *return true if valid
 *return false if not valid */

bool StatefulFirewall::check_if_valid_handshake(Connection &con,const Packet *p,int oldact)
{
	unsigned long seq = 0;
	unsigned long ack = 0;
	unsigned long seq_s = 0;
	unsigned long seq_d = 0;
	//  0 A <- B X SYN
	//  Y A -> B X+1 SYNACK
	//  Y+1 A <- B 0

	//  X A -> B 0 SYN
	//  X+1 A <- B Y SYNACK
	//  0 A -> B Y+1
	
	// wrong header 
	if(!p->has_network_header()) return false;
	
	const click_ip *iph = p->ip_header();
	uint8_t pr = iph->ip_p; 
	
	//no tcp
	if(pr != IP_PROTO_TCP) return false;
	const click_tcp *tcph = p->tcp_header();


	//last SYN req
	if(con.get_handshake_stat() == SYN)
	{						
		// NOT SYN ACK
		if((tcph->th_flags & (TH_ACK|TH_SYN)) != (TH_ACK|TH_SYN)) return false;	
		if(tcph->th_flags & TH_RST) return false;
		if(tcph->th_flags & TH_FIN) return false;
		//click_chatter("I am a good SYNACK");

		
	}
	//last SYNACK
	if(con.get_handshake_stat() == SYNACK)
	{
		// NOT ACK
		if(tcph->th_flags & TH_ACK != TH_ACK) return false;	
		if(tcph->th_flags & TH_RST) return false;
		if(tcph->th_flags & TH_FIN) return false;
		if(tcph->th_flags & TH_SYN) return false;
		//click_chatter("I am a good ACK");
	}

	
	// check sequence number 
	seq= ntohl(tcph->th_seq);
	ack= ntohl(tcph->th_ack);
	if(con.is_forward()){
		if(ack-1!=con.get_sourceseq()){
			return false;
		}
	}
	else{
		if(ack-1!=con.get_destseq()){
			return false;
		}
	}
	//click_chatter("I am a good SEQUENCE NUMBER");
	// valid handshake then update conn
	if(con.is_forward()){
		con.set_sourceseq(ack);
		con.set_destseq(seq);
	}
	else{
		con.set_sourceseq(seq);
		con.set_destseq(ack);
	}
	delete_connection(con);
	con.update_forward();
	con.update_handshake_stat(con.get_handshake_stat()+1);
	add_connection(con,oldact);
	return true;


}
void StatefulFirewall::deal_with_bad_handshake(Connection &con)

{
	//click_chatter("********BAD CONNECTION*********");
	//the default rule is accept
	if(DEFAULTACTION==1)
	{
		//change Connection table action value to 0
		delete_connection(con);
		con.update_handshake_stat(HS_DONE);
		add_connection(con,0);
	}

	//the default rule is block
	else
	{
		//delete Connection from table
		delete_connection(con);
	}
}

/*Check if the packet represent Connection reset
 * i.e., if the RST flag is set in the header.
 * Return true if connection reset
 * else return false.*/
bool StatefulFirewall::check_if_connection_reset(const Packet *p)
{
	if(!p->has_network_header()) return false;
	const click_ip *iph = p->ip_header();
	uint8_t pr = iph->ip_p; 
	if(pr == IP_PROTO_TCP)
	{	
		if(!p->has_transport_header())
		{
			return false;
		}
		const click_tcp *tcph = p->tcp_header();
		if( tcph->th_flags & TH_RST) return true;
	}
	return false;

}

/* Add a new connection to the map along with its action.*/
void StatefulFirewall::add_connection(Connection &c, int action)
{
	Connections.insert(std::pair<Connection,int>(c,action));
	//click_chatter("WOW----- -----");
	//c.print();
}

/* Delete the connection from map*/
void StatefulFirewall::delete_connection(Connection &c)
{
		std::map<Connection,int>::iterator it;
		for(it = Connections.begin(); it != Connections.end(); it++ )
		{
			if(it->first == c) {
				Connections.erase(it);
				break;
			}	
		}
}

/* Check if Packet belongs to new connection.
* If new connection, apply the policy on this packet
* and add the result to the connection map.
* Else return the action in map.
* If Packet indicates connection reset,
* delete the connection from connection map.
*
* Return 1 if packet is allowed to pass
* Return 0 if packet is to be discarded
*/
int StatefulFirewall::filter_packet(const Packet *p)
{
	int action;
	Connection cnt = get_canonicalized_connection(p);
	//click_chatter("-----PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP----");
	//click_chatter("-----STEP 1 :GET CONNECTION FROM PACKET-----");
	//cnt.print();
	if(!check_if_new_connection(p))
	{
		// two cases:
		// 1: No connection is found but not a SYN packet. default action
		action = DEFAULTACTION;

		// 2: Old connection found	
		//click_chatter("-----STEP 3-1 :GET ACTION FROM OLD CONNS-----");
		std::map<Connection,int>::iterator it;
		Connection c;
		int oldact = -1;
		for(it = Connections.begin(); it != Connections.end(); it++ )
		{
			//if old connection found
			if(it->first == cnt)
			{
				c = it->first;
				oldact = it->second;
				break;		
			}
		}
		if(oldact!=-1){
			if(c.get_handshake_stat() == HS_DONE) {
				action = oldact;
				//reset
				if(check_if_connection_reset(p)){
					delete_connection(c);
				}

			}
			else if(check_if_valid_handshake(c,p,oldact)) action = oldact;
			else {
				deal_with_bad_handshake(c);
				action = 0;
			}
			
		} 
		
		

	}
	else
	{
		
		//find policy
		//cnt.print();
		//click_chatter("-----STEP 3-2 :GET ACTION FROM POLICIES-----");
		action = DEFAULTACTION;
		for(vector<Policy>::iterator it = list_of_policies.begin(); it!=list_of_policies.end();it++)
		{
			Policy p = *it;
			if(p.getConnection()==cnt)
			{
				if(p.getConnection().is_forward()==cnt.is_forward()){
					//click_chatter("----STEP 4 :GET ACTION MATCH POLICY-----");
					action = p.getAction();
				}				
			}
			
		}
		//click_chatter("WOW-----STEP 5 :ADD CONNECTION -----");
		add_connection(cnt,action);
		

	}
	
	return action;

}
/* Push valid traffic on port 1
* Push discarded traffic on port 0*/
void StatefulFirewall::push(int port, Packet *p)
{
	//valid traffic
	
	port = filter_packet(p);
	//click_chatter("--------Filtered port:%d--------",port);
	output(port).push(p);


}

CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)
